const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    { email: String, fullname: String, phone: String, role: String },
    { timestamps: true }
);

schema.statics.findOneOrCreate = async function (user, role = 'staff') {
    const found = await this.findOne({ email: user.email, role });
    if (found) {
        return found;
    }
    return this.create(user);
};

module.exports = mongoose.model('customer', schema);
