const mongoose = require('mongoose');
const schema = new mongoose.Schema(
    {
        content: String,
        sender: String,
        room: String,
        customer: Boolean,
        type: { type: String, default: 'text' },
        file: { type: { id: String, name: String } }
    },
    { timestamps: true }
);

module.exports = mongoose.model('message', schema);
