const mongoose = require('mongoose');

const schema = new mongoose.Schema({ customer: String, name: String, lastMessage: String }, { timestamps: true });
schema.statics.findOneOrCreate = async function (user) {
    const found = await this.findOne({ customer: user.email });
    if (found) {
        return found;
    }
    return this.create({ customer: user.email, name: user.fullname });
};
module.exports = mongoose.model('room', schema);
