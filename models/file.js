const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    filename: String,
    originalname: String,
    encoding: String,
    mimetype: String,
    size: Number,
    destination: String,
    path: String
});
module.exports = mongoose.model('file', schema);
