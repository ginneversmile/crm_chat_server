const _ = require('lodash');
const messageModel = require('../../models/message');
const roomModel = require('../../models/room');
const fileModel = require('../../models/file');
module.exports = init;

function init(io) {
    io.on('connection', socket => {
        const user = socket.user;
        const role = user.role;
        if (role === 'customer') {
            console.log(`customer ${user.email} is connected`);
            socket.on('message', customer_message(io, socket));
            socket.on('load', customer_load(io, socket));
            socket.on('user', (fn = _.noop) => fn(user));
            socket.on('file', customer_file(io, socket));
        } else {
            console.log(`staff ${user.email} is connected`);
            socket.on('message', staff_message(io, socket));
            socket.on('load', staff_load(io, socket));
            socket.on('file', staff_file(io, socket));
        }
        socket.on('disconnect', reason => console.log(`user: ${user.email} is disconnected: ${reason}`));
    });
}

function staff_file(io, socket) {
    return async ({ id, room }, fn = _.noop) => {
        const email = socket.user.email;
        const file = await fileModel.findById(id);
        if (!file) {
            return;
        }
        const end_message = {
            room: room,
            type: 'file',
            sender: email,
            customer: false,
            file: { name: file.originalname, id: file._id }
        };
        await save_message(end_message);

        io.to(room).emit('file', end_message);
        io.to('staff').emit('file', end_message);
        fn();
    };
}

function customer_file(io, socket) {
    return async ({ id }, fn = _.noop) => {
        const email = socket.user.email;
        const room = socket.room._id.toString();
        const file = await fileModel.findById(id);
        if (!file) {
            return;
        }
        const end_message = {
            room: room,
            type: 'file',
            sender: email,
            customer: true,
            file: { name: file.originalname, id: file._id }
        };
        await save_message(end_message);

        io.to(room).emit('file', end_message);
        io.to('staff').emit('file', end_message);
        fn();
    };
}

function customer_load(_, socket) {
    const roomObj = socket.room;
    const room = roomObj._id.toString();
    return async (_, fn = _.noop) => {
        try {
            const messages = await messageModel.find({ room }).lean();
            return fn(messages);
        } catch (e) {
            /* handle error */
            console.error(e);
            return fn([]);
        }
    };
}

function staff_load() {
    return async (room, fn = _.noop) => {
        try {
            const messages = await messageModel.find({ room }).lean();
            return fn(messages);
        } catch (e) {
            /* handle error */
            console.error(e);
            return fn([]);
        }
    };
}

function customer_message(io, socket) {
    return async (message, fn = _.noop) => {
        const room = socket.room;
        const content = message.content;
        const email = socket.user.email;
        const room_id = room._id.toString();
        const end_message = { room: room._id, content, sender: email, customer: true, type: 'text' };
        await save_message(end_message);

        io.to(room_id).emit('message', end_message);
        io.to('staff').emit('message', end_message);
        return fn();
    };
}

function staff_message(io, socket) {
    return async (message, fn = _.noop) => {
        const room = message.room;
        const content = message.content;
        const email = socket.user.email;
        const end_message = { room, content, sender: email, customer: false, type: 'text' };
        await save_message(end_message);
        io.to(room).emit('message', end_message);
        io.to('staff').emit('message', end_message);
        return fn();
    };
}

async function save_message(end_message) {
    return Promise.all([
        messageModel.create(end_message),
        roomModel.findOneAndUpdate(
            { _id: end_message.room },
            { lastMessage: end_message.type === 'text' ? end_message.content : 'Tệp đính kèm' }
        )
    ]);
}
