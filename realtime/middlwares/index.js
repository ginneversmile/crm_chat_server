const cookie = require('cookie');
const jwt = require('../../jwt');
const roomModel = require('../../models/room');
module.exports = init;
function init(io) {
    io.use(parse_cookie);
    io.use(session);
    io.use(room);
}

async function session(socket, next) {
    let sid;
    if (socket.handshake.query?.mode === 'web') {
        sid = socket.cookies.scid;
    } else {
        sid = socket.cookies.cid;
    }
    if (!sid) {
        return next('session not found');
    }
    try {
        socket.user = await jwt.verifyToken(sid);
        return next();
    } catch (e) {
        return next(e);
    }
}

async function room(socket, next) {
    const user = socket.user;
    if (user.role === 'customer') {
        try {
            const room = await roomModel.findOneOrCreate(user);
            // console.log(room);
            socket.room = room;
            socket.join(room._id.toString());
        } catch (e) {
            console.error(e);
            /* handle error */
            return next(e);
        }
    } else {
        socket.join('staff');
    }
    return next();
}

function parse_cookie(socket, next) {
    let raw_cookie = socket.request.headers.cookie;
    socket.cookies = cookie.parse(raw_cookie);
    next();
}
