module.exports = init;
function init(server) {
    const io = require('socket.io')(server);
    require('./middlwares/index')(io);
    require('./handler/index')(io);
}
