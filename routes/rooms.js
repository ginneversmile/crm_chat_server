const router = require('express').Router();
const roomModel = require('../models/room');
const customerModel = require('../models/customer');
router.get('/', async (_, res) => {
    try {
        const rooms = await roomModel.find({});
        return res.json(rooms);
    } catch (e) {
        /* handle error */
        console.error(e);
        return res.json([]);
    }
});
router.get('/:room', async (req, res) => {
    try {
        const room = await roomModel.findOne({ _id: req.params.room }).lean();
        if (!room) {
            return res.status(404).end();
        }
        const customer = await customerModel.findOne({ email: room.customer }).lean();
        room.customerDetail = customer;
        return res.send(room);
    } catch (e) {
        console.error(e);
        return res.status(500).end();
    }
});
module.exports = router;
