const express = require('express');
const jwt = require('../jwt');
const router = express.Router();
const staffModel = require('../models/staff');
const customerModel = require('../models/customer');

/* GET users listing. */
router.get('/', async function (req, res) {
    const cid = req.cookies.scid;
    if (!cid) {
        return res.status(401).send({ code: 401 });
    }
    try {
        const payload = await jwt.verifyToken(cid);
        const user = await staffModel.findOneOrCreate(payload);
        return res.status(201).send({ code: 201, data: user });
    } catch (e) {
        console.log(e);
        /* handle error */
        return res.status(401).send({ code: 401 });
    }
});

router.post('/customer', async function (req, res) {
    const body = req.body;
    body.role = 'customer';
    try {
        await customerModel.findOneOrCreate(body, 'customer');
        const token = await jwt.genToken(body);
        res.cookie('cid', token);
        return res.status(201).send(token);
    } catch (e) {
        return res.status(500).send(e);
    }
});
router.post('/staff', async function (req, res) {
    const body = req.body;
    body.role = 'staff';
    try {
        await staffModel.findOneOrCreate(body);
        const token = await jwt.genToken(body);
        res.cookie('scid', token);
        return res.status(201).send(token);
    } catch (e) {
        return res.status(500).send(e);
    }
});

module.exports = router;
