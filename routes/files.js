const router = require('express').Router();
const multer = require('multer');
const fs = require('fs');
fs.access('uploads/', fs.constants.F_OK, err => {
    if (err) {
        fs.mkdir('uploads/', err => {
            if (err) {
                console.error(err);
            }
        });
    }
});
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/');
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now());
    }
});
const upload = multer({ storage });
const fileModel = require('./../models/file');
router.post('/', upload.single('file'), async (req, res) => {
    if (req.file) {
        const _file = req.file;
        const file = await fileModel.create(_file);
        return res.send(file);
    } else {
        return res.status(400).end();
    }
});
router.get('/:id', async (req, res) => {
    const id = req.params.id;
    const file = await fileModel.findById(id);
    if (!file) {
        return res.status(404).end();
    } else {
        return res.download(file.path);
    }
});
module.exports = router;
