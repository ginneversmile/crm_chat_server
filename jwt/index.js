const jwt = require('jsonwebtoken');
const secret = process.env.SECRET_KEY || 'hihi';
module.exports.genToken = genToken;
module.exports.verifyToken = verifyToken;
function genToken(args) {
    return new Promise((resolve, reject) => {
        return jwt.sign(args, secret, (err, token) => {
            if (err) {
                return reject(err);
            } else {
                return resolve(token);
            }
        });
    });
}

function verifyToken(token) {
    return new Promise((resolve, reject) => {
        return jwt.verify(token, secret, (err, payload) => {
            if (err) {
                return reject(err);
            } else {
                return resolve(payload);
            }
        });
    });
}
