const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const roomsRouter = require('./routes/rooms');
const filesRouter = require('./routes/files');
mongoose.connect(
    'mongodb://localhost/crmchat',
    { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false },
    error => console.log('connected', error)
);

const app = express();


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/chat/users', usersRouter);
app.use('/chat/rooms', roomsRouter);
app.use('/chat/files', filesRouter);

module.exports = app;
